use strict;
use warnings;


my $x = "This is a string";
print $x, "\n";


$x = 6;
print $x, "\n";

my @array = ("Me", 6, 7,8);
print @array, "\n";
print $array [1], "\n";


my @array2 = ([1,2,3], [4,5,6], [7,8,9]);
print $array2 [2][1], "\n";
print @{$array2[1]}, "\n";


my %grades = ("Me" => 80, "Ali" => 50, "Ayse" =>90);
print $grades{"Me"}, "\n";


my @array3 = ('one', 'two', 'three');
my $a = @array3;
print $a, "\n";
my ($b, $c, $d) = @array3;
print '$b$c$d', "\n";
print "$b$c$d", "\n";
print $b.$c.$d, "\n";
